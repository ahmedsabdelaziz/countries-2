import type {NextPage} from 'next';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useDispatch, auth} from '../app';
import {usePublicPage} from '../hooks/pages';
import InputPassword from '../components/InputPassword';

const initialValues = {username: '', password: '', remember: false};
const validationSchema = Yup.object({
  username: Yup.string()
    .trim()
    .min(3, 'Username should be at least 3 characters long')
    .max(30, 'Username should be at most 30 characters long')
    .required('Required'),
  password: Yup.string()
    .trim()
    .min(8, 'Password should be at least 8 characters long')
    .max(30, 'Password should be at most 30 characters long')
    .required('Required'),
});

const Login: NextPage = () => {
  usePublicPage();
  const dispatch = useDispatch();

  return (
    <div className="h-screen flex justify-center items-center bg-gray-50">
      <div
        className="
          w-full md:w-2/3 lg:w-1/2 xl:w-1/3
          p-4
          bg-white
          shadow-lg
          rounded-sm
          divide-y
          "
      >
        <h1 className="text-2xl font-semibold">Sign in</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            dispatch(auth.login(values));
          }}
        >
          {({values, touched, errors, handleChange, handleBlur, handleSubmit}) => (
            <form className="pt-8" onSubmit={handleSubmit}>
              <div className="flex">
                <label htmlFor="username" className="w-1/3 flex justify-end items-start">
                  <span className="text-red-600">*</span>
                  Username:&nbsp;
                </label>
                <div className="w-2/3 relative">
                  <input
                    type="text"
                    id="username"
                    name="username"
                    className={`
                      rounded-sm
                      w-full
                      px-1
                      mb-8
                      ring-2
                      focus-visible:ring-primary
                      ${
                        touched.username && errors.username
                          ? 'ring-red-600'
                          : 'ring-gray-200 hover:ring-gray-300'
                      }
                      `}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.username}
                  />
                  <div className="absolute bottom-2 text-red-600">
                    {touched.username && errors?.username}
                  </div>
                </div>
              </div>
              <div className="flex">
                <label htmlFor="password" className="w-1/3 flex justify-end items-start">
                  <span className="text-red-600">*</span>
                  Password:&nbsp;
                </label>
                <div className="w-2/3 relative">
                  <InputPassword
                    type="text"
                    id="password"
                    name="password"
                    className={`
                      rounded-sm
                      w-full
                      px-1
                      mb-8
                      ring-2
                      ${
                        touched.password && errors.password
                          ? 'ring-red-600'
                          : 'ring-gray-200 hover:ring-gray-300'
                      }
                      focus-within:ring-primary
                      hover:focus-within:ring-primary
                      `}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                  />
                  <div className="absolute bottom-2 text-red-600">
                    {touched.password && errors?.password}
                  </div>
                </div>
              </div>
              <div className="flex justify-end mb-8">
                <div className="w-2/3">
                  <label>
                    <input
                      type="checkbox"
                      name="remember"
                      className="accent-primary"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      checked={values.remember}
                    />
                    &nbsp;Remember me
                  </label>
                </div>
              </div>
              <div className="flex justify-end">
                <button type="submit" className="py-2 px-4 bg-primary text-white rounded-sm">
                  Login
                </button>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Login;
