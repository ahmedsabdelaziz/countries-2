import {useState} from 'react';
import type {NextPageWithLayout} from '../type/NextPageWithLayout';
import type {Country} from '../type/Country';
import {useGetAllCountriesQuery} from '../app';
import {useProtectedPage} from '../hooks/pages';
import ProtectedLayout from '../components/ProtectedLayout';
import ComponentTransition from '../components/ComponentTransition';
import Spinner from '../components/Spinner';
import CountriesList from '../components/CountriesList';
import CountryNameDialog from '../components/CountryNameDialog';

const Home: NextPageWithLayout = () => {
  useProtectedPage();

  const {data, isLoading} = useGetAllCountriesQuery(undefined);

  const [selectedName, setSelectedName] = useState<Country['name'] | null>(null);

  return (
    <ComponentTransition
      componentKey={isLoading.toString()}
      className="w-full h-full flex justify-center items-center relative"
    >
      {isLoading ? <Spinner /> : <CountriesList data={data!} onNameClick={setSelectedName} />}
      <CountryNameDialog name={selectedName} onClose={() => setSelectedName(null)} />
    </ComponentTransition>
  );
};

Home.Layout = ProtectedLayout;

export default Home;
