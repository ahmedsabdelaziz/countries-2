import {useMemo} from 'react';
import {useRouter} from 'next/router';
import type {NextPageWithLayout} from '../type/NextPageWithLayout';
import {useGetAllCountriesQuery} from '../app';
import ProtectedLayout from '../components/ProtectedLayout';
import ComponentTransition from '../components/ComponentTransition';
import Spinner from '../components/Spinner';
import CountryDetails from '../components/CountryDetails';

const Country: NextPageWithLayout = () => {
  const router = useRouter();
  const {code} = router.query;

  const {data, isLoading} = useGetAllCountriesQuery(undefined);
  const country = useMemo(() => data?.find((country) => country.cca2 == code), [data, code]);

  return (
    <ComponentTransition
      componentKey={isLoading.toString()}
      className="w-full h-full flex justify-center items-center relative"
    >
      {isLoading || !country ? <Spinner /> : <CountryDetails country={country} />}
    </ComponentTransition>
  );
};

Country.Layout = ProtectedLayout;

export default Country;
