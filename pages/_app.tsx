import type {AppProps} from 'next/app';
import {Provider} from 'react-redux';
import type {NextPageWithLayout} from '../type/NextPageWithLayout';
import {store} from '../app';
import ComponentTransition from '../components/ComponentTransition';
import PublicLayout from '../components/PublicLayout';
import '../styles/globals.css';

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};
function MyApp({Component, pageProps, router}: AppPropsWithLayout) {
  const Layout = Component.Layout || PublicLayout;

  return (
    <Provider store={store}>
      <ComponentTransition componentKey={Layout.key}>
        <Layout>
          <ComponentTransition componentKey={router.route} className="w-full h-full">
            <Component {...pageProps} />
          </ComponentTransition>{' '}
        </Layout>
      </ComponentTransition>
    </Provider>
  );
}

export default MyApp;
