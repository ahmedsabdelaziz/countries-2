module.exports = {
  mode: 'jit',
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#433D8A',
        },
        accent: {
          DEFAULT: '#F3E7DD',
        },
      },
      animation: {
        'spin-slow': 'spin 2s linear infinite',
        dash: 'dash 1.5s ease-in-out infinite',
      },
      keyframes: {
        dash: {
          '0%': {
            strokeDasharray: '1, 150',
            strokeDashoffset: '0',
          },
          '50%': {
            strokeDasharray: '90, 150',
            strokeDashoffset: '-35',
          },
          '100%': {
            strokeDasharray: '90, 150',
            strokeDashoffset: '-124',
          },
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
