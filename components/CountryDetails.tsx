import {ArrowLeftIcon} from '@heroicons/react/outline';
import Link from 'next/link';
import type {Country} from '../type/Country';

type Props = {
  country: Country;
};
export default function CountryDetails({country}: Props) {
  return (
    <div className="w-full h-full">
      <div className="w-full flex items-center gap-8 mb-8">
        <Link href="/">
          <a className="py-2 px-8 rounded-md shadow-xl text-gray-800 hover:text-primary transition-colors">
            <ArrowLeftIcon className="w-4 inline-block" />
            &nbsp; Back
          </a>
        </Link>
        <h2 className="font-semibold text-xl">{country.name.common}</h2>
      </div>
      <div className="flex border-b border-gray-200 mb-2">
        <div className="w-full md:w-1/3 p-2 flex justify-center items-center">
          <img src={country?.flags.svg} alt={country.name.common} className="w-full" />
        </div>
        <div className="w-full md:w-2/3 flex flex-wrap">
          <div className="w-full lg:w-1/2 flex flex-wrap py-2 px-4">
            <h4 className="w-full capitalize font-semibold">&nbsp;</h4>
            <div className=" w-1/4">Common:</div>
            <div className="w-3/4 font-light">{country?.name.common}</div>
            <div className="w-1/4">Official:</div>
            <div className="w-3/4 font-light">{country?.name.official}</div>
          </div>
          {Object.entries(country?.name.nativeName || {}).map(([lang, {common, official}]) => (
            <div key={lang} className="w-full lg:w-1/2 flex flex-wrap py-2 px-4">
              <h4 className="w-full capitalize font-semibold">{lang}</h4>
              <div className=" w-1/4">Common:</div>
              <div className="w-3/4 font-light">{common}</div>
              <div className="w-1/4">Official:</div>
              <div className="w-3/4 font-light">{official}</div>
            </div>
          ))}
        </div>
      </div>
      <div className="border-b border-gray-200 mb-2">
        <h3 className="text-lg font-semibold">Codes:</h3>
        <div className="flex flex-wrap pl-11">
          <div className=" w-1/4">Alpha 2:</div>
          <div className="w-3/4 font-light">{country?.cca2}</div>
          <div className=" w-1/4">Alpha 3:</div>
          <div className="w-3/4 font-light">{country?.cca3}</div>
          <div className=" w-1/4">Numeric:</div>
          <div className="w-3/4 font-light">{country?.ccn3}</div>
        </div>
      </div>
      <div className="mb-2">
        <h3 className="text-lg font-semibold">Currencies:</h3>
        <div className="pl-11 font-light">
          {Object.values(country?.currencies || {})
            .map(({name, symbol}) => `${name} ${symbol ? `(${symbol})` : ''}`)
            .join(', ')}
        </div>
      </div>
      <div>
        <h3 className="text-lg font-semibold">Languages:</h3>
        <div className="pl-11 font-light">{Object.values(country?.languages || {}).join(', ')}</div>
      </div>
    </div>
  );
}
