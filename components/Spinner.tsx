export default function Spinner() {
  return (
    <svg className="animate-spin-slow w-12 text-primary" viewBox="0 0 50 50">
      <circle
        className="animate-dash stroke-current"
        cx="25"
        cy="25"
        r="20"
        fill="none"
        strokeWidth="5"
      ></circle>
    </svg>
  );
}
