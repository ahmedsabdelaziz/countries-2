import type {Layout} from '../type/NextPageWithLayout';

const PublicLayout: Layout = ({children}) => {
  return <>{children}</>;
};

PublicLayout.key = 'public';

export default PublicLayout;
