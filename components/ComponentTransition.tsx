import {AnimatePresence, domAnimation, LazyMotion, m} from 'framer-motion';

const transition = {
  variants: {
    initial: {
      opacity: 0,
    },
    animate: {
      opacity: 1,
    },
    exit: {
      opacity: 0,
    },
  },
  transition: {
    duration: 0.3,
  },
};

type Props = Parameters<typeof m.div>[0] & {
  componentKey: string;
};
export default function ComponentTransition({children, componentKey, ...divProps}: Props) {
  const isClient = typeof window != 'undefined';

  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence exitBeforeEnter>
        <m.div
          key={componentKey}
          initial={!isClient && 'initial'}
          animate="animate"
          exit="exit"
          variants={transition.variants}
          transition={transition.transition}
          {...divProps}
        >
          {children}
        </m.div>
      </AnimatePresence>
    </LazyMotion>
  );
}
