import {HTMLProps, useState} from 'react';
import {EyeIcon, EyeOffIcon} from '@heroicons/react/outline';

type Props = HTMLProps<HTMLInputElement>;
export default function InputPassword({className, ...props}: Props) {
  const [visible, setVisible] = useState(false);

  return (
    <div className={`${className} flex items-center`}>
      <input {...props} type={visible ? 'text' : 'password'} className="grow ring-0" />
      <button onClick={() => setVisible((current) => !current)}>
        {visible ? <EyeIcon className="h-4" /> : <EyeOffIcon className="h-4" />}
      </button>
    </div>
  );
}
