import {useMemo, useState} from 'react';
import {Transition} from '@headlessui/react';
import {useDispatch, auth, useSelector} from '../../app';
import debounce from '../../lib/debounce';

export default function UserActions() {
  const username = useSelector((store) => store.auth.username || '');
  const dispatch = useDispatch();

  // When the user hovers from the trigger to the menu, the 'mouseleave' event is triggered and
  // then the 'mouseenter', and the menu flickers
  // Debouncing the state setter with a small timeout solves the issue
  const [dropdownState, origninalSetDropDownState] = useState<'open' | 'closed'>('closed');
  const setDropDownState = useMemo(
    () => debounce(origninalSetDropDownState, 100),
    [origninalSetDropDownState],
  );

  return (
    <div
      className="relative"
      tabIndex={0}
      onMouseEnter={() => setDropDownState('open')}
      onFocus={() => setDropDownState('open')}
      onMouseLeave={() => setDropDownState('closed')}
      onBlur={() => setDropDownState('closed')}
    >
      <div
        className="
          inline-flex
          p-2
          rounded-full
          uppercase text-sm text-white
          bg-primary
          bg-opacity-100 hover:bg-opacity-80
          transition-all
          duration-200
          "
      >
        {username.slice(0, 2)}
      </div>
      <Transition
        show={dropdownState == 'open'}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <div
          className="
            absolute right-0 origin-top-right
            w-56 mt-1
            rounded-md
            bg-white
            shadow-lg
            ring-1 ring-black ring-opacity-5
            divide-y
            "
        >
          <div className="w-full p-2 overflow-x-hidden whitespace-nowrap text-ellipsis">
            {username}
          </div>
          <div>
            <button
              className="w-full p-2 rounded-md align-middle text-sm"
              onClick={() => dispatch(auth.logout())}
            >
              Logout
            </button>
          </div>
        </div>
      </Transition>
    </div>
  );
}
