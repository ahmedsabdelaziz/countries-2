import Link from 'next/link';
import type {Layout} from '../../type/NextPageWithLayout';
import UserActions from './UserActions';

const ProtectedLayout: Layout = ({children}) => {
  return (
    <div className="min-h-screen flex flex-col">
      <div className="flex justify-center bg-accent">
        <div className="container py-4 px-2 flex justify-between items-center">
          <h1 className="text-2xl font-semibold">
            <Link href="/">
              <a>Countries</a>
            </Link>
          </h1>
          <UserActions />
        </div>
      </div>
      <main className="grow flex justify-center">
        <div className="container py-4 px-2">{children}</div>
      </main>
    </div>
  );
};

ProtectedLayout.key = 'protected';

export default ProtectedLayout;
