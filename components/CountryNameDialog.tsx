import {Fragment} from 'react';
import {Dialog, Transition} from '@headlessui/react';
import type {Country} from '../type/Country';

type Props = {
  name: Country['name'] | null;
  onClose: () => void;
};
export default function CountryNameDialog({name, onClose}: Props) {
  return (
    <Transition appear show={name != null} as={Fragment}>
      <Dialog as="div" className="fixed inset-0 z-10 overflow-y-auto" onClose={onClose}>
        <div className="min-h-screen px-4 text-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="inline-block h-screen align-middle" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
              <Dialog.Title as="h3" className="text-lg font-semibold leading-6 text-gray-900">
                {name?.common}
              </Dialog.Title>
              <div className="mt-2">
                <div className="flex flex-col gap-4 divide-y">
                  <div className="flex flex-wrap">
                    <div className=" w-1/4">Common:</div>
                    <div className="w-3/4 font-light">{name?.common}</div>
                    <div className="w-1/4">Official:</div>
                    <div className="w-3/4 font-light">{name?.official}</div>
                  </div>
                  {Object.entries(name?.nativeName || {}).map(([lang, {common, official}]) => (
                    <div key={lang} className="flex flex-wrap">
                      <h4 className="w-full capitalize font-semibold">{lang}</h4>
                      <div className=" w-1/4">Common:</div>
                      <div className="w-3/4 font-light">{common}</div>
                      <div className="w-1/4">Official:</div>
                      <div className="w-3/4 font-light">{official}</div>
                    </div>
                  ))}
                </div>
              </div>
              <div className="mt-4 flex justify-end">
                <button
                  type="button"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-primary rounded-md hover:bg-primary/90 transition-colors"
                  onClick={onClose}
                >
                  Close
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );
}
