import Link from 'next/link';
import {
  ColumnDirective,
  ColumnsDirective,
  Filter,
  GridComponent,
  Group,
  Inject,
  Page,
  PageSettingsModel,
  Sort,
} from '@syncfusion/ej2-react-grids';
import {EyeIcon} from '@heroicons/react/outline';
import {Country} from '../type/Country';

const pageSettings: PageSettingsModel = {
  pageSize: 10,
};

type Props = {
  data: Country[];
  onNameClick: (name: Country['name']) => void;
};
export default function CountriesList({data, onNameClick}: Props) {
  return (
    <GridComponent dataSource={data} allowPaging={true} pageSettings={pageSettings}>
      <ColumnsDirective>
        <ColumnDirective field="cca2" headerText="Alpha2" width="100" textAlign="Center" />
        <ColumnDirective
          headerText="Common name"
          template={function nameTemplate(country: Country) {
            return <button onClick={() => onNameClick(country.name)}>{country.name.common}</button>;
          }}
        />
        <ColumnDirective
          field="capital"
          headerText="Capital"
          template={function capitalTemplate(country: Country) {
            return country.capital?.join(', ') || 'N/A';
          }}
        />
        <ColumnDirective
          headerText="Actions"
          width="100"
          textAlign="Center"
          template={function actionsTemplate(country: Country) {
            return (
              <div className="flex justify-center">
                <Link href={`/${country.cca2}`}>
                  <a className="hover:text-primary transition-colors">
                    <EyeIcon className="w-5" />
                  </a>
                </Link>
              </div>
            );
          }}
        />
      </ColumnsDirective>
      <Inject services={[Page, Sort, Filter, Group]} />
    </GridComponent>
  );
}
