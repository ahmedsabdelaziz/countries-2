import {useEffect, useLayoutEffect} from 'react';

// In server side rendering useLayoutEffect throws an error
// To solve this, useIsomorphicLayoutEffect is a work around
// by using useEffect on the server, and switching to useLayoutEffect
// on the client
export default typeof document !== 'undefined' ? useLayoutEffect : useEffect;
