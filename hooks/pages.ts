import {useRouter} from 'next/router';
import {useSelector} from '../app';
import useIsomorphicLayoutEffect from './useIsomorphicLayoutEffect';

export function useProtectedPage() {
  const {state, username} = useSelector((store) => ({
    state: store.auth.state,
    username: store.auth.username,
  }));
  const {pathname, push} = useRouter();

  useIsomorphicLayoutEffect(() => {
    if (state == 'ready' && !username && pathname != '/login') {
      push('/login');
    }
  }, [state, username, pathname, push]);
}

export function usePublicPage() {
  const {state, username} = useSelector((store) => ({
    state: store.auth.state,
    username: store.auth.username,
  }));
  const {pathname, push} = useRouter();

  useIsomorphicLayoutEffect(() => {
    if (state == 'ready' && username && pathname == '/login') {
      push('/');
    }
  }, [state, username, pathname, push]);
}
