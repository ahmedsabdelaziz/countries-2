import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {Country} from '../type/Country';

const countriesApi = createApi({
  reducerPath: 'countriesApi',
  baseQuery: fetchBaseQuery({baseUrl: 'https://restcountries.com/v3.1/'}),
  endpoints: (builder) => ({
    getAllCountries: builder.query<Country[], undefined>({
      query: () => 'all',
    }),
  }),
});

const {reducerPath: name, useGetAllCountriesQuery, reducer, middleware} = countriesApi;
export {name, useGetAllCountriesQuery, reducer, middleware};
