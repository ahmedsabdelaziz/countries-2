import {createSlice, PayloadAction, Middleware, AnyAction} from '@reduxjs/toolkit';

type State = {
  state: 'loading' | 'ready';
  username: string | null;
};

const initialState: State = {
  state: 'loading',
  username: null,
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    init: (state) => {
      state.state = 'ready';
    },
    login: (
      state,
      {payload: {username}}: PayloadAction<{username: string; password: string; remember: boolean}>,
    ) => {
      state.username = username;
    },
    logout: (state) => {
      state.username = null;
    },
  },
});

const middleware: Middleware = (store) => (next) => (action: AnyAction) => {
  if (typeof window == 'undefined') {
    return next(action);
  }

  // Get user data from localStorage
  if (slice.actions.init.match(action)) {
    const remember = localStorage.getItem('remember') == 'true';
    if (remember) {
      const username = localStorage.getItem('username');
      const password = localStorage.getItem('password');

      if (username && password) {
        store.dispatch(slice.actions.login({username, password, remember}));
      }
    }
  }

  // Save user data to localStorage
  if (slice.actions.login.match(action)) {
    localStorage.setItem('username', action.payload.username);
    localStorage.setItem('password', action.payload.password);
    localStorage.setItem('remember', String(action.payload.remember));
  }

  // Delete user data from localStorage
  if (slice.actions.logout.match(action)) {
    localStorage.removeItem('username');
    localStorage.removeItem('password');
    localStorage.removeItem('remember');
  }

  return next(action);
};

const {name, actions, reducer} = slice;
export type {State};
export {name, actions, reducer, middleware};
