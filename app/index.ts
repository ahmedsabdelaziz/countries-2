import {configureStore} from '@reduxjs/toolkit';
import {setupListeners} from '@reduxjs/toolkit/query/react';
import {
  TypedUseSelectorHook,
  useDispatch as useOriginalDispatch,
  useSelector as useOriginalSelector,
} from 'react-redux';
import * as auth from './auth';
import * as countries from './countries';

export const store = configureStore({
  reducer: {
    [auth.name]: auth.reducer,
    [countries.name]: countries.reducer,
  },
  middleware: (getDefaultMiddlewares) =>
    getDefaultMiddlewares().concat(auth.middleware, countries.middleware),
});

store.dispatch(auth.actions.init());
setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useDispatch = () => useOriginalDispatch<AppDispatch>();
export const useSelector: TypedUseSelectorHook<RootState> = useOriginalSelector;

// Re-export action creators to make a single entry point for the app module
const {actions: authActions} = auth;
const {useGetAllCountriesQuery} = countries;
export {authActions as auth, useGetAllCountriesQuery};
