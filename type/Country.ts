export type Country = {
  cca2: string;
  cca3: string;
  ccn3: string;
  name: {
    common: string;
    official: string;
    nativeName: {
      [language: string]: {
        common: string;
        official: String;
      };
    };
  };
  capital?: string[];
  region: string;
  subregion: string;
  flags: {
    png: string;
    svg: string;
  };
  area: number;
  currencies: {
    [key: string]: {
      name: string;
      symbol: string;
    };
  };
  languages: {
    [key: string]: string;
  };
};
