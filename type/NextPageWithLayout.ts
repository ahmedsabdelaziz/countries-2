import {ComponentType, PropsWithChildren} from 'react';
import {NextPage} from 'next';

export type Layout = ComponentType<PropsWithChildren<{}>> & {key: string};
export type NextPageWithLayout = NextPage & {
  Layout?: Layout;
};
